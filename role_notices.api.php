<?php

/**
 * @file
 * Hooks specific to the Role Notices module.
 *
 * If a module creates it's own hook's it should declare them in this file. The
 * functions in this file are never actually invoked but can be used by other
 * developers to learn the signatures of the module's hooks.
 *
 * IDEs like PHPStorm also scan these files in order to create auto-completion
 * of hook functions.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform alterations on notices.
 *
 * @param array $notices
 *   The notices where the keys are the role ids and the values are the notices
 *   text.
 */
function hook_role_notices_alter(&$notices) {
  // Get all authenticated user roles.
  $roles = user_roles(TRUE);
  foreach ($notices as $rid => $notice_text) {
    if ($roles[$rid]->isAdmin()) {
      $notices[$rid] = t("For admin eyes only:") . ' ' . $notices[$rid];
    }
  }
}

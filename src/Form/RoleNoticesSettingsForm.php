<?php

namespace Drupal\role_notices\Form;

use Drupal\Core\Form\FormBase;
use Drupal\role_notices\NoticesManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple Settings form.
 *
 * All classes that we use as forms must implement
 * \Drupal\Core\Form\FormInterface. Here we extending \Drupal\Core\Form\FormBase
 * because it implements some of this interfaces methods and provides other
 * useful functionality.
 *
 * This settings form does not save it's settings as config(they need to be
 * updated on the same site environment site) so it does not extend
 * Drupal\Core\Form\ConfigFormBase.
 */
class RoleNoticesSettingsForm extends FormBase {

  /**
   * The notices manager service.
   *
   * @var \Drupal\role_notices\NoticesManager
   */
  protected $noticesManager;

  /**
   * Constructs a RoleNoticesSettingsForm object.
   *
   * @param \Drupal\role_notices\NoticesManager $notices_manager
   *   The notices manager for getting and setting notices.
   */
  public function __construct(NoticesManager $notices_manager) {
    $this->noticesManager = $notices_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Create function to provide our necessary service.
   *
   * This works because we are extending FormBase which implements
   * ContainerInjectionInterface.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container that lets us get our service.
   *
   * @link https://www.drupal.org/node/2133171 For more on dependency injection
   */
  public static function create(ContainerInterface $container) {
    return new static(
    /*
     *  Load the service required to construct this class.
     *  Notice that this is the name we used to define our service in
     *  role_notices.services.yml.
     */
      $container->get('role_notices.notice_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_notices_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $role_names = user_role_names(TRUE);
    /*
     * Using an empty container with #tree property
     *  will make our values come back as array.
     */
    $form['notices'] = [
      '#tree' => TRUE,
    ];
    $notices = $this->noticesManager->getAllNotices();
    // Create 1 text area for each role.
    foreach ($role_names as $role_id => $role_name) {
      $form['notices'][$role_id] = [
        '#type' => 'textarea',
        '#title' => $role_name,
        '#description' => $this->t('Add a notice for the <strong>@role</strong> role.', ['@role' => $role_name]),
        '#default_value' => isset($notices[$role_id]) ? $notices[$role_id] : '',
      ];
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Notices'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $notices = $form_state->getValue('notices');
    $this->noticesManager->setAllNotices($notices);
    \Drupal::messenger()->addMessage($this->t('The notices have been saved.'));
  }

}

<?php

namespace Drupal\Tests\role_notices;

use Drupal\Tests\BrowserTestBase;

/**
 * Basic functionality test.
 *
 * Currently this test is not meant to teach writing tests. It is used to make
 * sure module functionality is not broken(like most tests). It is not
 * guaranteed to pass for each stage of the module.
 *
 * @group role_notices
 */
class RoleNoticesTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['role_notices'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->drupalCreateRole([], 'test_role');
  }

  /**
   * Test basic module functionality.
   */
  public function testNotices() {
    $assert = $this->assertSession();
    $this->drupalGet('admin/reports/role_notices');
    $assert->pageTextNotContains('Hello World');
    $assert->pageTextContains('Access denied');
    $notices_admin = $this->createUser(['administer role notices']);
    $this->drupalLogin($notices_admin);
    $notices_admin->addRole('test_role');
    $notices_admin->save();
    $this->drupalGet('admin/reports/role_notices');
    $assert->pageTextContains('Hello World');

    $notice = 'important message';
    $this->setNotice('test_role', $notice);

    // @todo test user profile view.
    $this->drupalPlaceBlock('role_notices');
    $this->drupalGet('');
    $assert->pageTextContains($notice);

    $this->drupalLogin($this->createUser([]));
    $this->drupalGet('');
    $assert->pageTextNotContains($notice);

    $this->drupalLogin($notices_admin);

    // Ensure that updating the message will update the block.
    $new_notice = "You've got mail.";
    $this->setNotice('test_role', $new_notice, $notice);
    $this->drupalGet('');
    $assert->pageTextNotContains($notice);
    $assert->pageTextContains($new_notice);

    $notices_admin->removeRole('test_role');
    $notices_admin->save();

    $this->drupalGet('');
    $assert->pageTextNotContains($notice);
    $assert->pageTextNotContains($new_notice);
  }

  /**
   * Tests accessing pages via links that take more permissions.
   */
  public function testNoticesAsAdmin() {
    $this->drupalPlaceBlock('local_tasks_block');
    $assert = $this->assertSession();
    $this->drupalLogin($this->createUser([
      'administer role notices',
      'administer users',
      'access administration pages',
      'access site reports',
    ]));
    $this->drupalGet('admin/reports');
    $this->clickLink('Role Notices - Hello World');
    $assert->pageTextContains('Hello World');

    $this->drupalGet('admin/people');
    $this->clickLink('Role Notices');

    $assert->fieldValueEquals("notices[authenticated]", '');
    $this->drupalPostForm(NULL, ["notices[authenticated]" => 'Hi there'], 'Save Notices');
    $assert->pageTextContains('The notices have been saved.');
    $assert->fieldValueEquals("notices[authenticated]", 'Hi there');
  }

  /**
   * Set notice via form.
   */
  private function setNotice($role, $notice, $existing_notice = '') {
    $assert = $this->assertSession();
    $this->drupalGet('admin/people/role_notices');

    $assert->fieldValueEquals("notices[$role]", $existing_notice);
    $this->drupalPostForm(NULL, ["notices[$role]" => $notice], 'Save Notices');
    $assert->pageTextContains('The notices have been saved.');
    $assert->fieldValueEquals("notices[$role]", $notice);
  }

}

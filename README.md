Roles Notices - A Drupal Learning Module
=======================

Project site: https://www.drupal.org/project/role_notices

Author: Ted Bowman
https://www.drupal.org/u/tedbow

What Is This?
-------------
This is a functional Drupal module that is used to teach Drupal module 
development.

It has been specifically designed to teach Drupal module development concepts.
There are Drupal 7 and Drupal 8 versions of this module.

Module Functionality
--------------------
The finished version of this module provides the ability to set a text message for each authenticated role on the site.
The roles can then be displayed on the user profile and in a block.

How to use:
-------
This repo has multiple branches for different versions of the module.
The latest branch is `8.x-4.x`

Each branch has corresponding git tags for each step in building the module. The latest version of the module uses the tag pattern `8.x-4.demo.[STEP#]`. It starts with tag `8.x-4.demo.1`. Every new tag in the list adds more functionality to the module. Checkout each tag in the list to see how the module progresses from a simple "Hello Word" page to a fully functional module.

For instance:

```
# Lists all tags in the current version
git tag -l 8.x-4.demo.*

# Checkout the first step of the module
git check 8.x-4.demo.1

# Look at files and read the in-code documentation and try out module functionality

# Checkout the second step in the module
git check 8.x-4.demo.2

# See what is new in this step
git diff 8.x-4.demo.1
```

If you don't use git see the [Project page](https://www.drupal.org/project/role_notices) for zipped folder for each module step.

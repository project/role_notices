<?php

namespace Drupal\role_notices\Plugin\Block;

/**
 * This is Plugin of the Block type.
 *
 * Drupal finds out about this block via annotations.
 *
 * If coming from Drupal 7 the annotations replace hook_block_info().
 * Build function replaces hook_block_view().
 *
 * *** IMPORTANCE OF AN IDE ***
 * In this class we will implement the interface:
 * ContainerFactoryPluginInterface
 * To do so we must implement the method "create".
 * If we don't do so this will be a fatal PHP error.
 *
 * If you are using a good IDE it will notify you are this error in your
 * workspace as you work.
 *
 * If you are using an IDE comment out the "create" function to see how your
 * IDE notifies you of this error.
 *
 * Also because we are extending the abstract class BlockBase which implements
 * BlockPluginInterface we must also implement the "build" method from
 * BlockPluginInterface.
 */

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\role_notices\NoticesManager;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Notices" block.
 *
 * Following part of comments is an annotation:
 *
 * @Block(
 *   id = "role_notices",
 *   admin_label = @Translation("Role Notices")
 * )
 *
 * @link https://drupal.org/node/1882526
 */
class RoleNoticesBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The Notices Manager service.
   *
   * @var \Drupal\role_notices\NoticesManager
   */
  protected $noticesManager;

  /**
   * Constructs a RNoticesBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\role_notices\NoticesManager $notices_manager
   *   The Role Notices NoticesManager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, NoticesManager $notices_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->noticesManager = $notices_manager;
  }

  /**
   * Creates an instance of the plugin.
   *
   * We must implement this because our class implements:
   *  \Drupal\Core\Plugin\ContainerFactoryPluginInterface.
   *
   * We implemented this class because we need a service from the container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('role_notices.notice_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // Since this module only provides message to authenticated users we only
    // access to authenticated users.
    return AccessResult::allowedIf($account->isAuthenticated());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /*
     * Without dependency injection, we could have accessed the NoticesManager
     * service directly using:
     * @code
     * return [
     *  '#theme' => 'item_list',
     *  '#items' => \Drupal::service('role_notices.notice_manager')->getUserNotices(),
     * ];
     * @endcode
     *
     */
    $notices = $this->noticesManager->getUserNotices();
    return [
      '#theme' => 'item_list',
      '#items' => $notices,
      '#cache' => [
        /*
         * Add a cache context so when the user's roles change the notices will
         * be correct for the new roles.
         */
        'contexts' => ['user.roles'],
        /*
         * Add cache tags so that this block view will be rebuilt when one of
         * the notices changes.
         *
         * @see Drupal\role_notices\NoticesManager::setAllNotices();
         */
        'tags' => $this->noticesManager->getRenderTags(array_keys($notices)),
      ],
    ];
  }

}

<?php

namespace Drupal\role_notices\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A test controller.
 *
 * The controller does not need to extend any other class or implement a
 * specific interface.
 *
 * We could optionally extend \Drupal\Core\Controller\ControllerBase if we
 * needed one of the many services it provides.
 *
 * @see \Drupal\Core\Controller\ControllerBase
 */
class RoleNoticesController {

  /*
   * We use this trait so that we have access to `$this->t()`.
   *
   * It is best practice to not use the global `t()` function in classes.
   */
  use StringTranslationTrait;

  /**
   * Page callback for '/role_notices'.
   *
   * You have to go to /role_notices to test.
   *
   * Render array documentation.
   * @link https://www.drupal.org/docs/8/api/render-api/render-arrays
   *
   * This connected to the path via the role_notices.routing.yml
   *
   * @return array
   *   Render array.
   */
  public function page() {
    $build = [
      '#type' => 'markup',
      // All non user input text displayed to the end user should be translated
      // via the t() function.
      '#markup' => $this->t('Hello World'),
    ];
    return $build;
  }

}
